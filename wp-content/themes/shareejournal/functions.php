<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function shareejournal_theme_scripts() {
  wp_enqueue_style( 'bng-font-style', get_stylesheet_directory_uri().'/assets/css/font-css.css' );
  wp_enqueue_style( 'custom-theme-style', get_stylesheet_directory_uri().'/assets/css/custom.css' );
 
  
}
add_action( 'wp_enqueue_scripts', 'shareejournal_theme_scripts' );

function wpdocs_theme_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri().'/assets/css/font-css.css' );
    add_editor_style( get_stylesheet_directory_uri().'/assets/css/custom-editor.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

add_action('wp_head','theme_add_google_tracking',100);

function theme_add_google_tracking(){
    ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131723509-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131723509-1');
</script>

<?php
}